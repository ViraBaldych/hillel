package ClassC;

import School.AllDataOfStudents;
import School.StudentExam;

public class DataOfStudentsC extends AllDataOfStudents {

    public DataOfStudentsC(String name, String surname, int age, String clas, String gender, StudentExam exam) {
        super(name, surname, age, clas, gender, exam);
    }


    public void displayInformation(){
        System.out.println(" name of student " + name + " family name " + surname + " age " + age + " is a " + clas + " is a " + " get exam  " + exam);
    }
}
