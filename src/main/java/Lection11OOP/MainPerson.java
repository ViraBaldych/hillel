package Lection11OOP;

import Lection12OOP.Display;
import Lection12OOP.Man;
import Lection12OOP.PersonRole;
import Lection12OOP.Women;

public class MainPerson {
    public static void main(String[] args) {
        try {
            Man bro = new Man(20, "Petro", "");
            System.out.println(bro);

            Man he = new Man(70, "Pedro", "");
            System.out.println(he);

            Man his = new Man(47, "Fedora", "");
            System.out.println(his);

            Women she = new Women(52, "Kate", "Doc");
            System.out.println(she);

        }catch (UserException e ) {
            System.out.println("Error: " + e.getMessage());
        }
        try{
            Women her = new Women(32, "Anna", "");
            System.out.println(her);
        } catch (UserException e ){
            System.out.println("some massage: " + e.getMessage());

        }
    }

}

