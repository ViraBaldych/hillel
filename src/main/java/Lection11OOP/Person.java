package Lection11OOP;


public class Person {
    public int age;
    public String name;
    public String profession;

    public Person(int age, String name, String profession) throws UserException {
        if (age < 0) {
            throw new UserException("age cannot be les than 0");
        }
        if (name == null || name.trim().isEmpty()) {
            throw new UserException("name cannot be empty you need to add name of user");
        }
        if (profession == null || profession.trim().isEmpty())
        throw new UserException("add your profession pls)");
    }

    public int getAge() {
        return age;
    }

    public String getProfession() {
        return profession;
    }

    public String getName() {
        return name;
    }

}
