package Lection11OOP;

public class UserException extends Exception {
    public UserException (String message) {
        super(message);
    }
}
