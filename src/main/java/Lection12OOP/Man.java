package Lection12OOP;

import Lection11OOP.Person;
import Lection11OOP.UserException;

public class Man extends Person {

    public Man(int age, String name, String profession) throws UserException {
        super(age, name, profession);
    }

}