package Lection12OOP;

public enum PersonRole {
    STUDENT,
    TEACHER,
    DOCTOR,
    ENGINEER,
    ARTIST,
    OTHER
}
