package Lection12OOP;

import Lection11OOP.Person;
import Lection11OOP.UserException;

public class Women extends Person {

    public Women(int age, String name, String profession) throws UserException {
        super(age, name, profession);
    }
}
