package Lection4HomeWork;

public class Radius {
        public static void main(String[] args) {
            double radius = 1.5;
            double height = 7.0;
            double volume = calculateCylinderVolume(radius, height);
            System.out.println("The volume of the cylinder with radius " + radius + " and height " + height + " is " + volume);
        }
        public static double calculateCylinderVolume(double radius, double height) {
            return Math.PI * radius * radius * height;
        }
    }
