package Lection6HomeWork;

public class AAxyzBB {
    public static void main(String[] args) {

        String a = "AAxyzBB"; //true
        String b = "AxyzBB"; //true
        String c = "AxyzBBB"; //false
        String d = "xyz"; //true
        xyzMiddle(a);
        xyzMiddle(b);
        xyzMiddle(c);
        xyzMiddle(d);
    }

    public static boolean xyzMiddle(String str) {
        boolean result;
        if (str.length() < 3) {
            System.out.println("length <3" + false);
            return false;
        }

        int middle = str.length() / 2;
        if (str.length() % 2 == 1) {
            result = str.substring(middle - 1, middle + 2).equals("xyz");

        } else {
            result = str.substring(middle - 2, middle + 1).equals("xyz");
        }
        System.out.println("middle of " + str + " is " + result);
        return result;

    }

}

