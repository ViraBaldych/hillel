package Lection6HomeWork;

public class Hello3Times {
        public static void main(String[] args) {
            System.out.println(repeatEnd("Hello", 3)); // Expected: "llolollo"
            System.out.println(repeatEnd("Hello", 2)); // Expected: "lolo"
            System.out.println(repeatEnd("Hello", 1)); // Expected: "o"
        }

        public static String repeatEnd(String str, int n) {
            return str.substring(str.length() - n).repeat(n);
        }


}