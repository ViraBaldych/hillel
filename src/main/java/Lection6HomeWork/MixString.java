package Lection6HomeWork;

public class MixString {
    public static void main(String [] args) {
        System.out.println(mixString("abc", "xyz"));
        System.out.println(mixString("Hi", "There"));
        System.out.println(mixString("xxxx", "There"));
    }
            public static String mixString(String a, String b) {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < Math.max(a.length(), b.length()); i++) {
                if (i < a.length()) result.append(a.charAt(i));
                if (i < b.length()) result.append(b.charAt(i));
            }
            return result.toString();
        }
    }
