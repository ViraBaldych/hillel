package Lection7HomeWork;

public class AverageArrayOpt1 {
    public static void main(String[] args) {
        double[] array = new double[]{12.3, 13.5, 33.4};
        double sum = 0.0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        double average = sum / array.length;
        System.out.println("Average array element: " + average);
    }
}
