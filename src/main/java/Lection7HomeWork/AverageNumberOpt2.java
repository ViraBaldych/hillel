package Lection7HomeWork;

public class AverageNumberOpt2 {
    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4, 5};
        int average = (int) calculateAverage(numbers);
        System.out.println("The average is: " + average);
    }

    public static double calculateAverage(int[] array) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException("Array will be not null or empty");
        }
        int sum = 0;
        for (int num : array) {
            sum += num;
        }

        return sum / array.length;
    }
}
