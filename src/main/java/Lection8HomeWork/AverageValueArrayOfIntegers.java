package Lection8HomeWork;

public class AverageValueArrayOfIntegers {
    public static void main(String[] args) {
        System.out.println(centeredAverage(new int[]{1, 2, 3, 4, 100})); //→ 3);
        System.out.println(centeredAverage(new int[]{1, 1, 5, 5, 10, 8, 7})); //→ 5);
        System.out.println(centeredAverage(new int[]{-10, -4, -2, -4, -2, 0})); //→ -3);
    }

    public static int centeredAverage(int[] nums) {
        int sum = 0;
        int min = nums[0];
        int max = nums[0];
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            if (nums[i] < min) min = nums[i];
            if (nums[i] > max) max = nums[i];
        }
        sum -= min;
        sum -= max;

        return sum / (nums.length -2);
    }
}
