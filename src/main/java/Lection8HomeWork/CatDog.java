package Lection8HomeWork;

public class CatDog {
    public static void main(String[] args){
        System.out.println(catDog("catdog")); //true
        System.out.println(catDog("catcat")); //false
        System.out.println(catDog("1cat1cadodog")); //true
    }
    public static  boolean catDog(String str) {
        int countCat = (str.length() - str.replace("cat", "").length());
        int countDog = (str.length() - str.replace("dog", "").length());
        return countDog == countCat;
    }
}
