package Lection8HomeWork;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Code {
    public static void main (String[] args){
        System.out.println(countCode("aaacodebbb")); // → 1;
        System.out.println(countCode("codexxcode")); // → 2;
        System.out.println(countCode("cozexxcope")); // → 3;

    }
    public static int countCode(String str ) {
        int count = 0;
        Pattern pattern = Pattern.compile("co.e");
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()){
           count++;
        }
        return count;
    }

}
