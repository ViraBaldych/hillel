package Lection8HomeWork;

public class ComparingTwoStrings {
    public static void main (String[] args) {
        System.out.println(endsWith("AbC", "HiaBc")); //→ true
        System.out.println(endsWith("abc", "abXabc")); //→ true
        System.out.println(endsWith("Hiabc", "abc")); //→ true
    }

        public static boolean endsWith(String str1, String str2) {

            String lowerStr1 = str1.toLowerCase();
            String lowerStr2 = str2.toLowerCase();
            return lowerStr1.endsWith(lowerStr2) || lowerStr2.endsWith(lowerStr1);
        }
    }
