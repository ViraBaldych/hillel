package Lection8HomeWork;

public class CountEvenInts {
    public static void main(String[] args) {
        System.out.println(countEvenInts(new int[]{2, 1, 2, 3, 4})); //3
        System.out.println(countEvenInts(new int[]{2, 2, 0})); //3
        System.out.println(countEvenInts(new int[]{1, 3, 5})); //0
    }

    public static int countEvenInts(int[] nums) {
        int count = 0;
        for (int num : nums) {
            //for (int i=0; i<nums.length; i++){
            if (num % 2 == 0) {
                count++;
            }
        }
        return count;
    }
}
