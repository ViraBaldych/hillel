package Lection8HomeWork;

public class DoubleElement {
    public static void main (String[] args){
        System.out.println(doubleChars("The"));// → "TThhee”
        System.out.println(doubleChars("AAbb"));// → "AAAAbbbb”
        System.out.println(doubleChars("Hi-There"));// → "HHii--TThheerree”
    }
    public static String doubleChars(String str) {
        StringBuilder result = new StringBuilder();
        for (int i=0; i<str.length(); i++){
            char ch = str.charAt(i);
            result.append(ch).append(ch);
        }
        return result.toString();
    }

}
