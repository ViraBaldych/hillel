package Lection8HomeWork;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Hi {
    public static void main (String[] args){
        System.out.println(countHi("abc hi ho"));//1
        System.out.println(countHi("ABChi hi"));//2
        System.out.println(countHi("hihi"));//2
    }
    public static int countHi(String str) {
        int count = 0;
        Pattern pattern = Pattern.compile("hi");
        Matcher matcher = pattern.matcher(str);
        while (matcher.find())
            count++;
        return count;
    }

}
