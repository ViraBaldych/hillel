package Lection8HomeWork;

public class ReturnSumNumbersArray {
    public static void main(String[] args) {
        System.out.println(sumIgnoreSections(new int[]{1, 2, 2})); //5
        System.out.println(sumIgnoreSections(new int[]{1, 2, 2, 6, 99, 99, 7})); //5
        System.out.println(sumIgnoreSections(new int[]{1, 1, 6, 7, 2})); //4
    }

    public static int sumIgnoreSections(int[] nums) {
        int sum = 0;
        boolean inSection = false;
        for (int num : nums) {
            if (num == 6) {
                inSection = true;
            } else if (num == 7 && inSection) {
                inSection = false;
            } else if (!inSection) {
                sum += num;

            }
        }
        return sum;


    }

}
