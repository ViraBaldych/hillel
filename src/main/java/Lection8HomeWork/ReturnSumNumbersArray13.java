package Lection8HomeWork;

public class ReturnSumNumbersArray13 {
    public static void main(String[] args) {
        System.out.println(sumWithoutUnlucky13(new int[]{1, 2, 2, 1}));// → 6;
        System.out.println(sumWithoutUnlucky13(new int[]{1, 1}));//2
        System.out.println(sumWithoutUnlucky13(new int[]{1, 2, 2, 1, 13}));//6
    }

    public static int sumWithoutUnlucky13(int[] nums) {
        int sum = 0;
        for (int i = 0; i < nums.length; i++)
            if (nums[i] == 13) {
                i++;
            } else {
                sum += nums[i];
            }
        return sum;


    }
}
