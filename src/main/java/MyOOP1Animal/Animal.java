package MyOOP1Animal;

import MyOOP2Animal.Cat;
import MyOOP2Animal.Dog;

public class Animal {
    public void animalSound(){
        System.out.println("sound animal");
    }
    public static void main(String[] args) {
        Animal myAnimal = new Animal();
        Animal myCat = new Cat();
        Animal myDog = new Dog();

        myAnimal.animalSound();
        myCat.animalSound();
        myDog.animalSound();
    }


}
