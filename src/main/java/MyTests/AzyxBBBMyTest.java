package MyTests;

public class AzyxBBBMyTest {
    public static void main(String[] args) {
        String a = "aViraBala"; //true 9 aaa
        String b = "a, Vra, Bal, a"; //false8
        String c = "aViraBalb"; //false9
        String d = "MuraMVarM"; //true9 MMM
        String e = "MuraMVarolM"; //false11
        String f = "MM";
        System.out.println("all three character of " + a + " is " + middle(a));
        System.out.println("all three character of " + b + " is " + middle(b));
        System.out.println("all three character of " + c + " is " + middle(c));
        System.out.println("all three character of " + d + " is " + middle(d));
        System.out.println("all three character of " + e + " is " + middle(e));
        System.out.println("all three character of " + f + " is " + middle(f));
        }


        /*
        1. check String size
        2. if it <3 return false
        3. convorted string to charArray
        4. get size of the cherArray(arrray.length)
        5. if charArray size is Odd (2,4,6,8...) false
        6. get first char of the Array
        7. get middle chat of the array
        8. get last char of the array
        9. compare 6,7,8

         */

    public static boolean middle(String str) {
        int length = str.length();
        if (length <3)
            return false;
        char[] array = str.toCharArray();
        int arrayLength = array.length;
        if(arrayLength  %2 ==0)
            return false;
        int middleIndex = arrayLength/2;
        char firstChar = array[0];
        char middleChar = array[middleIndex];
        char lastChar = array[arrayLength -1];
        boolean isEqual = firstChar == middleChar && firstChar == lastChar;
        return isEqual;
        }
}
