package MyTests;

import java.util.ArrayList;
import java.util.List;

public class Maps {
    public static void main(String[] args) {
        System.out.println(mergeList());

    }

    public static List<Integer> mergeList() {
        List<Integer> list1 = List.of(1, 2, 3);
        List<Integer> list2 = List.of(3, 4, 5);
        int x = 0;
        int y = 0;

        List<Integer> result = new ArrayList<>();

        while (x < list1.size() && y < list2.size()) {
            if (list1.get(x) <= list2.get(y)) {
                result.add(list1.get(x));
            } else {
                result.add(list2.get(y));
            }
        }
        return result;
    }

}
