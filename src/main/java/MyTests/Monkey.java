package MyTests;

public class Monkey {
        public static void main(String[] args) {

            System.out.println("monkeyTrouble(true,true) " + monkeyTrouble(true,true));
            System.out.println("monkeyTrouble(false,false) " + monkeyTrouble(false,false));
            System.out.println("monkeyTrouble(true,false) " + monkeyTrouble(true,false));
            System.out.println("monkeyTrouble(false,true) " + monkeyTrouble(false,true));

        }
        public static boolean monkeyTrouble(boolean aSmile, boolean bSmile) {
            return aSmile == bSmile;
        }
    }

