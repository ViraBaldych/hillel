package MyTests;

public class SquareNumber {
    public static void main(String[] args) {
        int number = 6;
        System.out.println("The square of " + number + " is " + squareNumber(number));
    }
    public static int squareNumber(int n) {
        return n * n;

    }
}