public class TwoIntegers {
        public static void main(String[] args) {
            int a = 4;
            int b = 7;
            int result = power(a, b);
            System.out.println(a + " raised to the power of " + b + " is " + result);
        }
        public static int power(int a, int b) {
            return (int) Math.pow(a, b);
        }
    }
