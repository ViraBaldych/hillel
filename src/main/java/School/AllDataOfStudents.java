package School;

public class AllDataOfStudents implements DisplayInfo {
        public String name;
        public String surname;
        public int age;
        public String clas;
        public String gender;
        public StudentExam exam;

    public AllDataOfStudents(String name, String surname, int age, String clas, String gender, StudentExam exam) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.clas = clas;
        this.gender = gender;
        this.exam = exam;

    }

    public StudentExam getExam() {
        return exam;
    }

    public void setExam(StudentExam exam) {
        this.exam = exam;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getClas() {
        return clas;
    }

    public void setClas(String clas) {
        this.clas = clas;
    }
    public void displayInformation(){
        System.out.println(" name of student "+ name + "family name" + surname + " get " + exam + " age "+ age + "is a " + clas + " is a " );
    }
}
