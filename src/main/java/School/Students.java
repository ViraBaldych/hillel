package School;

import ClassA.DataOfStudentsA;
import ClassB.DataOfStudentsB;
import ClassC.DataOfStudentsC;

import static School.StudentExam.*;

public class Students {
    public static void main(String[] args) {

        DataOfStudentsA aa = new DataOfStudentsA("Anna", "Adams",19,"A","W",HISTORY);
        DataOfStudentsA ab = new DataOfStudentsA("Peter", "Smith",18,"A","M",MATH);
        DataOfStudentsA ac = new DataOfStudentsA("John", "Smith",17,"A","M",LANGUAGES);
        DataOfStudentsB ba = new DataOfStudentsB("Jase", "Bond",18,"B","W", LAV);
        DataOfStudentsB bb = new DataOfStudentsB("Mary", "And",19,"B","W", ECONOMY);
        DataOfStudentsC ca = new DataOfStudentsC("Kate", "Peters",17,"C","W", CHEMISTRY);
        DataOfStudentsC cb = new DataOfStudentsC("Antonio", "Bandera",18,"C","M", ECONOMY);
        aa.displayInformation();
        ab.displayInformation();
        ac.displayInformation();
        ba.displayInformation();
        bb.displayInformation();
        ca.displayInformation();
        cb.displayInformation();

    }
}
