package lection10Maps;

import java.util.LinkedHashSet;

public class ABLinkedHashSet7 {
    public static void main(String[] args) {
        LinkedHashSet<Integer> SetA = new LinkedHashSet<>();
        SetA.add(1);
        SetA.add(2);
        SetA.add(3);
        LinkedHashSet<Integer> SetB = new LinkedHashSet<>();
        SetB.add(3);
        SetB.add(4);
        SetB.add(5);
        System.out.println(containtsAllElement(SetA, SetB));
    }

    public static boolean containtsAllElement(LinkedHashSet<Integer> SetA, LinkedHashSet<Integer> SetB) {
        for (Integer element : SetB) {
            if (!SetA.contains(element)) {
                return false;
            }
        }
        return true;
    }
}
