package lection10Maps;

import java.util.HashMap;
import java.util.Map;

public class CountWord3 {
    public static void main(String[] args){
        System.out.println(countWord(new String[]{"a", "b", "a", "c", "b"}));
        System.out.println(countWord(new String[]{"c", "b", "a"}));
        System.out.println(countWord(new String[]{"c", "c", "c", "c"}));

    }
    public static Map<String, Integer> countWord (String[] strings){
      Map<String, Integer> wordCountMap = new HashMap<>();
      for (String str : strings) {
          if (wordCountMap.containsKey(str)) {
              wordCountMap.put(str, wordCountMap.get(str) +1);
          }else{
              wordCountMap.put(str, 1);
          }
      }
      return wordCountMap;
    }
}
