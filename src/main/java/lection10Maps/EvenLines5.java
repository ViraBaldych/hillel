package lection10Maps;

import java.util.HashMap;
import java.util.Map;

public class EvenLines5 {
    public static void main(String[] args) {
        System.out.println(buildResultString(new String[]{"a", "b", "a"})); // → "a"
        System.out.println(buildResultString(new String[]{"a", "b", "a", "c", "a", "d", "a"})); // → "a"
        System.out.println(buildResultString(new String[]{"a", "", "a"})); // → "a"
    }

    public static String buildResultString(String[] strings) {
        Map<String, Integer> Map = new HashMap<>();
        StringBuilder result = new StringBuilder();

        for (String str : strings) {
           Map.put(str, Map.getOrDefault(str, 0) + 1);
            if (Map.get(str) % 2 == 0) {
                result.append(str);
            }
        }

        return result.toString();
    }
}