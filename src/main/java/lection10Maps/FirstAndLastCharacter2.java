package lection10Maps;

import java.util.HashMap;
import java.util.Map;

public class FirstAndLastCharacter2 {
    public static void main(String[] args) {
        System.out.println(CreateMap(new String[]{"code", "bug"}));
        System.out.println(CreateMap(new String[]{"man", "mood", "main"}));
        System.out.println(CreateMap(new String[]{"man", "mood", "good", "night"}));
    }

    public static Map<String, String> CreateMap(String[] strings) {
        Map<String, String> map = new HashMap<>();
        for (String str : strings) {
            if (!str.isEmpty()) {
                String firstChar = String.valueOf(str.charAt(0));
                String lastChar = String.valueOf(str.charAt(str.length() - 1));
                map.put(firstChar, lastChar);
            }
        }
        return map;
    }
}
