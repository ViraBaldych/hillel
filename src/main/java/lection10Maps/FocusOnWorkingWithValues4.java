package lection10Maps;

import java.util.HashMap;
import java.util.List;

public class FocusOnWorkingWithValues4 {
    public static void main(String[] args) {
        List<Integer> list = List.of(3, 1, 3, 2, 2, 3, 4, 3);
        System.out.println(findMostFrequentElement(list));
    }

    public static int findMostFrequentElement(List<Integer> list) {
        HashMap<Integer, Integer> frequencyMap = new HashMap<>();
        for (int num : list) {
            frequencyMap.put(num, frequencyMap.getOrDefault(num, 0) + 1);
        }
        int mostFrequent = list.get(0);
        int maxCount = 0;

        for (int num : frequencyMap.keySet()) {
            if (frequencyMap.get(num) > maxCount) {
                maxCount = frequencyMap.get(num);
                mostFrequent = num;
            }

        }
        return mostFrequent;
    }

}
