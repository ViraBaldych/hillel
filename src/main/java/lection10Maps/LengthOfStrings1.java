package lection10Maps;

import java.util.HashMap;
import java.util.Map;

public class LengthOfStrings1 {
    public static void main(String[] args){
        String[] array1 = {"a", "bb", "a", "bb"};
        String[] array2 = {"this", "and", "that", "and"};
        String[] array3 = {"code", "code", "code", "bug"};
        System.out.println(getLengthOfStrings(array1));
        System.out.println(getLengthOfStrings(array2));
        System.out.println(getLengthOfStrings(array3));
//why we dont use sout?
//what is mean this case?
    }
    public static Map<String, Integer> getLengthOfStrings(String[] strings){
        Map<String, Integer> map = new HashMap<>();
        for (String str : strings) {
            map.put(str, str.length());
        }
        return map;
    }
}
