package lection10Maps;

import java.util.HashMap;
import java.util.Map;

public class ReturnElement6 {
    public static void main(String[] args){
        int[] nums = {1, 2, 3, 4, 2, 1};
        System.out.println(findMostFrequentElement(nums));
    }
    public  static int findMostFrequentElement (int [] nums){
        Map<Integer,Integer> Map= new HashMap<>();
        int mostFrequent = nums[0], maxCount = 0;

        for (int num : nums) {
            int count = Map.getOrDefault(num, 0) + 1;
            Map.put(num, count);
            if (count > maxCount) {
                maxCount = count;
                mostFrequent = num;

            }
        }
        return mostFrequent;
    }
}
