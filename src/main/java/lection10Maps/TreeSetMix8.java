package lection10Maps;
import java.util.TreeSet;
public class TreeSetMix8 {
    public static void main(String[] args){

        TreeSet<Integer> setA = new TreeSet<>();
        setA.add(1);
        setA.add(2);
        setA.add(3);

        TreeSet<Integer> setB = new TreeSet<>();
        setA.add(3);
        setA.add(4);
        setA.add(5);

        TreeSet<Integer> setC = new TreeSet<>();
        setC.addAll(setA);
        setC.addAll(setB);
        System.out.println("" + setC);
    }
}
