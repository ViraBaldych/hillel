package lection10Maps;

import java.util.HashMap;
import java.util.Map;

public class WorkingWithValues {
    public static void main(String[] args){
        System.out.println(mergeStringsByFirstChar(new String[]{"salt", "tea", "soda", "toast"}));
        System.out.println(mergeStringsByFirstChar(new String[]{"aa", "bb", "cc", "aAA", "cCC", "d"}));
        System.out.println(mergeStringsByFirstChar(new String[]{}));

    }
    public static Map<String, String> mergeStringsByFirstChar(String[] str){
        Map<String, String> result = new HashMap<>();

        for (String s : str) {
            String firstChar = s.substring(0,1);

            if (result.containsKey(firstChar)) {
                result.put(firstChar, result.get(firstChar) +s);
            }else{
                result.put(firstChar, s);
            }
        }
        return result;
    }

}
