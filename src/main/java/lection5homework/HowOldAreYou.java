package lection5homework;

import java.util.Scanner;

public class HowOldAreYou {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("How old are you:");
        int age = scanner.nextInt();

        String result = checkAge(age);
        System.out.println(result);

        scanner.close();
    }

    public static String checkAge(int age) {

        if (age >= 18) {
            return "You are an adult, you can drink";
        } else {
            return "You are not an adult, no no no";
        }
    }
}
