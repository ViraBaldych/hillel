package lection5homework;

import java.util.Scanner;

public class NumbersFrom1toN {
    public static void main (String[] args) {
        Scanner scanner = new Scanner (System.in);

        System.out.println("Enter number N: ");
        int N = scanner.nextInt();

        String result = calculateSumUPToN(N);
        System.out.println(result);
        scanner.close();
    }
    public static String calculateSumUPToN(int N) {
        if (N < 1) {
            return "invalid input parameters";
        }
        int sum = 0;
        StringBuilder result = new StringBuilder();
        for (int i = 1; i <= N; i++) {
            sum += i;
            result.append(i);
            if (i < N) {
                result.append("+");
            }
        }
        result.append("=").append(sum);
        return result.toString();
    }

}
