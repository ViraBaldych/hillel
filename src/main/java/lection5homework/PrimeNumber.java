package lection5homework;

public class PrimeNumber {
    public static void main(String[] args) {
        System.out.println("isPrime(7) → " + isPrime(7));
        System.out.println("isPrime(0) → " + isPrime(0));
        System.out.println("isPrime(8) → " + isPrime(8));

    }
    public static boolean isPrime(int n) {
        if (n <= 1) {

      //  for (int i = 2; i < n; i++) {
         //   if (n % i == 0) {
             return false;
            }
        for (int i = 2; i <= Math.sqrt(n); i++){
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
