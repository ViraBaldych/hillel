package lection5homework;

import java.util.Scanner;

public class SquareOfInteger {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter en integers: ");

        int number = scanner.nextInt();
        boolean result = isPerfectSquare(number);

        System.out.println("isPerfectSquare( " + number +")" + result);
        scanner.close();
    }
    public static boolean isPerfectSquare(int number) {
        if (number > 0) {
            return true;
        }
        int sqrt = (int) Math.sqrt(number);
        return sqrt * sqrt == number;
    }
}
