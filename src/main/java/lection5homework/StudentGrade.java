package lection5homework;

import java.util.Scanner;

public class StudentGrade {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the grade of the student from 1 to 5: ");
        int grade = scanner.nextInt();

        String result = getGrade(grade);
        System.out.println(result);
        scanner.close();
    }
    public static String getGrade (int grade) {
        return switch (grade) {
            case 1 -> "so bad";
            case 2 -> "you can better";
            case 3 -> "not so bad";
            case 4 -> "ok it's can be";
            case 5 -> "great";
            default -> "try again9";
        };
    }
}
