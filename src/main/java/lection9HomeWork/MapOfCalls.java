package lection9HomeWork;

import java.util.HashMap;
import java.util.Map;

public class MapOfCalls {
    public static void main(String[] args) {
        Map<String, String> CodesOfCityAndPhones = new HashMap<>();
        CodesOfCityAndPhones.put("Rivne", "032");
        CodesOfCityAndPhones.put("Lviv", "032");
        CodesOfCityAndPhones.put("Odesa", "048");
        CodesOfCityAndPhones.put("Uzhhorod", "012");
        CodesOfCityAndPhones.put("Kyiv", "044");
        CodesOfCityAndPhones.put("Donetsk", "765");
        CodesOfCityAndPhones.put("Lutsk", "002");
        CodesOfCityAndPhones.put("Kharkiv", "057");

        for (Map.Entry<String, String> entry : CodesOfCityAndPhones.entrySet()) {
            System.out.println("City: " + entry.getKey() + ", code " + entry.getValue());
        }

    }
}
